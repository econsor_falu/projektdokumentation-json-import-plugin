<?php 


require_once(ABSPATH . 'wp-load.php');
/**
 * Class for custom searching builds
 */
class ECSearcher
{
    /**
     * summary
     */
    public function __construct()
    {
        
    }


    /**
     * case insensitive search within weekly overviews
     * @param int $id Post id
     * @param String $search Searchquery
     * @param bool $detailedSearch If results are displayed on detail-search (default,true) or global-search (false)
     */
    function getDetailedWeekoverview($id, $search, $detailedSearch = true) {

    	$results = array();
		$currentPost = get_post($id);

		$content = apply_filters('the_content', get_post_field('post_content', $id));

		$dom = new DOMDocument;
		$dom->loadHTML('<?xml encoding="utf-8" ?>' .$content);
		$allNodes = $dom->getElementById('wo-accordeon')->getElementsByTagName('section');

		foreach($allNodes as $node){
	    	$firstPTag = $node->getElementsByTagName('p')->item(0);

	    	$reg 	= '/'.str_replace(" ","(.)*",strtolower(htmlspecialchars_decode ($search))).'/';

		    if (preg_match($reg,strtolower($firstPTag->nodeValue)))
		    { 
		        $heading = $firstPTag->nodeValue;

		        $r = array('"', '&');
		        $s = array(' quot ', ' amp ');

		        $url = $currentPost->guid.'#'.slugify(str_replace($r, $s, $heading)); 

		        $excerpt = $this->trimText($node->getElementsByTagName('p')->item(1)->nodeValue);

		        $html = '<div class="week-searchresult';
		        $html .= ($detailedSearch? '-detailed ': '-global global-result ');
		        $html .= '">
							<div class="found-title">';
				$html .= ($detailedSearch? '': '<h2>');
				$html .= '	<a href="'.$url.'">'.$heading.'</a>';
				$html .= ($detailedSearch? '': '</h2>');
				$html .= '</div>
							<div class="weekoverview-title">
								<span>/ <a href="'.$currentPost->guid.'">'.$currentPost->post_title.'</a></span>
							</div>

							<div class="weekoverview-excerpt">
								<p>'.$excerpt.'</p>
							</div>
							
						</div>';
				

		        array_push($results, $html);

		    }
		}

		return $results;

	}

	/*
	* trim given text nicely 
	*
	*/
	function trimText($str, $strlenwidth = 160) {

		$str_ret = $str;

		if (strlen($str_ret) > $strlenwidth)
		{
			$str_ret = wordwrap($str_ret, 160);
		    $i = strpos($str_ret, "\n");
		    if ($i) {
		        $str_ret = substr($str_ret, 0, $i);
		    }
		    $str_ret .= ' ...';
		} 
		return $str_ret;
	}


	function displayWeekOverview($title, $url) {
		$html = '<div class="week-searchresult">
					<div class="found-title">';
		$html .= '<h2>';
		$html .= '	<a href="'.$url.'">'.$title.'</a>';
		$html .= '</h2>';
		$html .= '	</div>					
				</div>';
		return $html;
	}


	/*
	* get first not empty p-element and display it as excerpt
	*
	*/
	function generateExcerpt($id) {
		$content = apply_filters('the_content', get_post_field('post_content', $id));
		$dom = new DOMDocument;	
		$dom->loadHTML('<?xml encoding="utf-8" ?>' .$content);
		$allNodes = $dom->getElementsByTagName('p');
		foreach ($allNodes as $value) {
			if(empty($value->nodeValue) || strpos($value->nodeValue, '{') !== false) continue;
			return $this->trimText($value->nodeValue);
		}
		return '';
	}

    

}
?>