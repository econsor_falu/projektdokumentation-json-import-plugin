<?php
/**
 * Plugin Name: Econsor: Wochenübersicht Import
 * Plugin URI: https://www.econsor.de
 * Description: Benutzerdefiniertes Plugin, um den Import der Wochenübersicht zu ermöglichen.
 * Version: 1.0.0
 * Author: Econsor
 * Author URI: https://www.econsor.de
 * Text Domain: ec-woi
 *
 * @package Econsor: Weekoverview Import
 * @since 0.1
 *
 * Copyright 2021 Econsor
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'ECONSOR_IMPORT_WEEKOVERVIEW_BASEDIR', plugin_dir_path( dirname( __FILE__ ) ) . 'econsor-import-weekoverview' );


require_once(ECONSOR_IMPORT_WEEKOVERVIEW_BASEDIR.'/class/WOIHandler.php');
require_once(ECONSOR_IMPORT_WEEKOVERVIEW_BASEDIR.'/tcpdf_min/tcpdf.php');
require_once(ECONSOR_IMPORT_WEEKOVERVIEW_BASEDIR.'/class/PDFGenerator.php');



function weekoverview_scripts() {
	global $post_type;
    if( 'weekly_overview' == $post_type ) {
      wp_enqueue_script( 'weekoverview', plugin_dir_url( __FILE__ ).'js/weekoverview.js', array('jquery'));
    }
}
add_action("wp_enqueue_scripts", "weekoverview_scripts");


function add_import_to_menu() {
		add_menu_page(
	        __( 'Import Wochenübersicht', 'textdomain' ),
	        'Import Wochenübersicht',
	        'wo_upload',
	        'econsor-import-weekoverview/templates/admin-view.php',
	        '',
	        '',
	        21
	    );
}
add_action( 'admin_menu', 'add_import_to_menu' );


function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

	setlocale(LC_ALL, 'de_DE');
  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}


function upload_woi() {
	$file = $_FILES['file'];

	$tmpurl 	= $file['tmp_name'];
	$filename 	= $file['name'];


	// Get the contents of the JSON file 
	$uploadedJson = file_get_contents($tmpurl);
	// Convert to array 
	$arr = json_decode($uploadedJson, true);


	if($file['error'] == UPLOAD_ERR_OK) {
		move_uploaded_file($tmpurl, ECONSOR_IMPORT_WEEKOVERVIEW_BASEDIR.'/imports/'.$filename);
	} else {
    	exit( wp_redirect( admin_url( 'admin.php?page=econsor-import-weekoverview%2Ftemplates%2Fadmin-view.php&info=failed' ) ) );
	}



	$parts = explode('-', $filename);

	if(is_numeric($parts[2])) {
		$title 	= "Wochenübersicht KW ".$parts[2]. " ".$parts[1];
		if(strtolower($parts[3]) == 'psd' || strtolower($parts[3]) == 'vp') {
			$title .= ' ('.strtoupper($parts[3]).') ';
		}
		$url 	= import($arr, $filename);
		$pdfs 	= generate($arr['data'], $title);
	} else {	
		$exkw 	= preg_replace('~\D~', '', $parts[2]);
		$title 	= "Wochenübersicht KW ".$exkw. " ".$parts[1];
		if(strtolower($parts[3]) == 'psd' || strtolower($parts[3]) == 'vp') {
			$title .= ' ('.strtoupper($parts[3]).') ';
		}
		$url 	= update($arr, $filename);
		$pdfs 	= generate($arr['data'], $title);
	}

	$upload_dir = wp_upload_dir();
	makeDir($upload_dir['basedir'].'/week_overview/'.slugify($title));
	$pdfs[0]->output($upload_dir['basedir'].'/week_overview/'.slugify($title).'/1.pdf','F');
	$pdfs[1]->output($upload_dir['basedir'].'/week_overview/'.slugify($title).'/2.pdf','F');
	exit( wp_redirect( $url ) );

}
add_action( 'admin_post_nopriv_upload_woi', 'upload_woi' );
add_action( 'admin_post_upload_woi', 'upload_woi' );



function import($arr, $filename) {
	$upload_dir = wp_upload_dir();
	$woi = new WOIHandler();
	global $user_ID;


	//create array for all tabs
	$tabsArr = array();
	$count = 0;
	foreach ($arr['data'] as $data) {
		if($data['info'] != "") {
			$countInfocard = 0;
			$count++;
			//info not empty, thema is heading, info is text before infocards
			$tabsArr[$count]['info'] = $data['info'];
			$tabsArr[$count]['header'] = $data['thema'];
		} else {
			//createInfoCard($heading,$date,$note,$tags,$action,$path,$target,$comment)
			$tabsArr[$count]['info'] = $tabsArr[$count]['info'] . $woi->createInfoCard($data['thema'],$data['termin'],$data['hinweis'][1],implode(', ',$data['tags']),$data['aktion'],implode(', ',$data['pfade']),$data['zielgruppe'],$data['kommentar'],$countInfocard);
			$countInfocard++;
		}
	}
	//construct based on enfold
	$tabs = "";
	$contentTable = "<div class='sticky-trigger hidden' style='position:relative;top:-280px;'><span>trigger</span></div><h2 class='heading-contenttable'>Inhaltsverzeichnis</h2><div class='content-table'>";
	foreach ($tabsArr as $key => $data) {		

		//Sanitize from single quotes
		$data['header'] = str_replace("'", "`", $data['header']);

		
		$tabs .= $woi->createToggleTab(makeUrltoLink($data['info']), $data['header'], slugify($data['header']));
		$str = $data['header'];
		$strlenwidth = 75;
		if (strlen($str) > $strlenwidth)
		{
		    $str = wordwrap($str, 75);
		    $i = strpos($str, "\n");
		    if ($i) {
		        $str = substr($str, 0, $i);
		    }
		}

		$str .= '...';


		$contentTable .= "<div class='content-entry'><span data-target='#".slugify($data['header'])."' class='content-nav'>".$str." <i class='far fa-angle-right'></i></span></div><br>";
	}
	$contentTable .= "</div>";
	$container = $woi->createToggleContainer($tabs);
	$parts = explode('-', $filename);

	$title = "Wochenübersicht KW ".$parts[2]. " ".$parts[1];

	if(strtolower($parts[3]) == 'psd' || strtolower($parts[3]) == 'vp') {
		$title .= ' - '.strtoupper($parts[3]);
		$wtype = (strtolower($parts[3]) == 'psd'  ? 10 : 11 );
	} else {
		$wtype = 12;
	}
	
	$pageheader = "<h1>".$title."</h1><p>
	Die Wochenübersicht steht Ihnen auch direkt im webCenter zur Verfügung. Öffnen sie in Ihrem webCenter-Mandanten im Ordner \"Kanalübergreifend\" das Dokument \"Wochenübersicht\" und selektieren Sie dort im Feld \"Wochenauswahl\" die passende KW (Kalenderwoche).

Im webCenter bietet Ihnen die Wochenübersicht den Komfort, mit einem Klick die betreffende Inhaltsseite zu einem Eintrag aufzurufen. Sie haben außerdem die Möglichkeit, Anmerkungen zu hinterlegen und die Einträge nach Ihrer Bearbeitung als erledigt zu markieren. Die Einträge einer Wochenübersicht lassen sich im webCenter darüber hinaus als Excel-Datei – inkl. Ihrer Bearbeitungsvermerke – exportieren.</p>";
	$pageheader .= '<div class="downloads">
						<h3>Download der PDF Dateien</h3>
						<div class="link">
							<a target="_blank" href="/wp-content/uploads/week_overview/'.slugify($title).'/1.pdf">PDF 1</a>
						</div>
						<div class="link">
							<a target="_blank" href="/wp-content/uploads/week_overview/'.slugify($title).'/2.pdf">PDF 2</a>
						</div>
					</div>';

	$heading = $woi->createHeading($pageheader);
	$section2 = $woi->layoutOneThird($contentTable);
	$section2 .= $woi->layoutTwoThird($container);

	$content = $woi->introsection($heading);
	$content .= $woi->section($section2);


	$kw = "0";
	if($parts[2] <= 5) {
			$kw = "1-5";
	}elseif(6 <= $parts[2] && $parts[2] <= 10) {
			$kw = "6-10";
	}elseif(11 <= $parts[2] && $parts[2] <= 15) {
			$kw = "11-15";
	}elseif(16 <= $parts[2] && $parts[2] <= 20) {
			$kw = "16-20";
	}elseif(21 <= $parts[2] && $parts[2] <= 25) {
			$kw = "21-25";
	}elseif(26 <= $parts[2] && $parts[2] <= 30) {
			$kw = "26-30";
	}elseif(31 <= $parts[2] && $parts[2] <= 35) {
			$kw = "31-35";
	}elseif(36 <= $parts[2] && $parts[2] <= 40) {
			$kw = "36-40";
	}elseif(41 <= $parts[2] && $parts[2] <= 45) {
			$kw = "41-45";
	}elseif(46 <= $parts[2] && $parts[2] <= 50) {
			$kw = "46-50";
	}elseif(51 <= $parts[2] && $parts[2] <= 53) {
			$kw = "51-53";
	}
	try {
		$new_post = array(
			"post_title" => $title,
			"post_content" =>$content,
			"post_status" => "publish",
			"post_date" => date("Y-m-d H:i:s"),
			"post_author" => $user_ID,
			"post_type" => "weekly_overview",
	        "page_template"  => "single-weekly_overview.php",
			"post_category" => array()
		);
		$post_id = wp_insert_post($new_post);

		$taxonomy = 'w_type';
		wp_set_object_terms($post_id, array($wtype), $taxonomy);

		$taxonomy = 'w_year';
		wp_set_object_terms($post_id, $parts[1], $taxonomy);

		$taxonomy = 'w_kw';
		wp_set_object_terms($post_id, $kw, $taxonomy);

		$taxonomy = 'w_month';
		$dates = $woi->convertCWeek($parts[2], $parts[1]);
		wp_set_object_terms($post_id, $woi->getMonth($dates['month']), $taxonomy);

	} catch (Exception $e) {
		exit( wp_redirect( admin_url( 'admin.php?page=econsor-import-weekoverview%2Ftemplates%2Fadmin-view.php&info=failed' ) ) );
	}

	try {
		$tree = ShortcodeHelper::build_shortcode_tree( $content );
	    AviaBuilder::set_alb_builder_status( 'active', $post_id );
	    AviaBuilder::save_posts_alb_content( $post_id, $content );
	    Avia_Builder()->element_manager()->get_elements_state( 'post', $post_id );
	    ShortcodeHelper::clean_up_shortcode( $_POST['_aviaLayoutBuilderCleanData'], 'content' );
	    update_post_meta( $post_id, '_avia_builder_shortcode_tree', $tree );			
	} catch (Exception $e) {
		exit( wp_redirect( admin_url( 'admin.php?page=econsor-import-weekoverview%2Ftemplates%2Fadmin-view.php&info=failed' ) ) );
	}


	return admin_url( 'admin.php?page=econsor-import-weekoverview%2Ftemplates%2Fadmin-view.php&info=success');

}

function update($arr, $filename) {
	$woi = new WOIHandler();
	global $user_ID;


	//create array for all tabs
	$tabsArr = array();
	$count = 0;
	foreach ($arr['data'] as $data) {
		if($data['info'] != "") {
			$countInfocard = 0;
			$count++;
			//info not empty, thema is heading, info is text before infocards
			$tabsArr[$count]['info'] = $data['info'];
			$tabsArr[$count]['header'] = $data['thema'];
		} else {
			//createInfoCard($heading,$date,$note,$tags,$action,$path,$target,$comment)
			$tabsArr[$count]['info'] = $tabsArr[$count]['info'] . $woi->createInfoCard($data['thema'],$data['termin'],$data['hinweis'][1],implode(', ',$data['tags']),$data['aktion'],implode(', ',$data['pfade']),$data['zielgruppe'],$data['kommentar'],$countInfocard);
			$countInfocard++;
		}
	}
	//construct based on enfold
	$tabs = "";
	$contentTable = "<div class='sticky-trigger hidden' style='position:relative;top:-280px;'><span>trigger</span></div><h2 class='heading-contenttable'>Inhaltsverzeichnis</h2><div class='content-table'>";
	foreach ($tabsArr as $key => $data) {
		$tabs .= $woi->createToggleTab($data['info'], $data['header'], slugify($data['header']));
		$str = $data['header'];
		$strlenwidth = 75;
		if (strlen($str) > $strlenwidth)
		{
		    $str = wordwrap($str, 75);
		    $i = strpos($str, "\n");
		    if ($i) {
		        $str = substr($str, 0, $i);
		    }
		}

		$str .= '...';


		$contentTable .= "<div class='content-entry'><span data-target='#".slugify($data['header'])."' class='content-nav'>".$str." <i class='far fa-angle-right'></i></span></div><br>";
	}
	$contentTable .= "</div>";
	$container = $woi->createToggleContainer($tabs);
	$parts = explode('-', $filename);

	$exkw = preg_replace('~\D~', '', $parts[2]);
	$title = "Wochenübersicht KW ".$exkw. " ".$parts[1];
	

	$pageheader = "<h1>".$title."</h1>";
	$heading = $woi->createHeading($pageheader);
	$section2 = $woi->layoutOneThird($contentTable);
	$section2 .= $woi->layoutTwoThird($container);

	$content = $woi->introsection($heading);
	$content .= $woi->section($section2);


	$page = get_page_by_title(html_entity_decode($title), OBJECT, 'weekly_overview');
	$post = array(
			'ID' => $page->ID,
			'post_content' => $content
		);
	wp_update_post($post);
	try {
		$tree = ShortcodeHelper::build_shortcode_tree( $content );
	    AviaBuilder::set_alb_builder_status( 'active', $page->ID );
	    AviaBuilder::save_posts_alb_content( $page->ID, $content );
	    Avia_Builder()->element_manager()->get_elements_state( 'post', $page->ID );
	    ShortcodeHelper::clean_up_shortcode( $_POST['_aviaLayoutBuilderCleanData'], 'content' );
	    update_post_meta( $page->ID, '_avia_builder_shortcode_tree', $tree );			
	} catch (Exception $e) {
		exit( wp_redirect( admin_url( 'admin.php?page=econsor-import-weekoverview%2Ftemplates%2Fadmin-view.php&info=failed' ) ) );
	}

	return admin_url( 'admin.php?page=econsor-import-weekoverview%2Ftemplates%2Fadmin-view.php&info=edit' );	
}

function generate($data, $title) {
	$pdfgen = new PDFGenerator($title, '', $data);
	$pdf1 = $pdfgen->generatePDF1($title, '', $data);	
	$pdf2 = $pdfgen->generatePDF2($title, '', $data);
	return array($pdf1,$pdf2);
}


function filter_function_name($input_object, $sfid)
{	
	if($sfid == 1897) {
		if($input_object["name"] == "_sft_w_kw") {
			$optionsarr = $input_object['options'];
			usort($optionsarr, function($a, $b) {
			    return $a->value - $b->value;
			});
			$input_object['options'] = $optionsarr;
		}
	}
  
	return $input_object;
}
add_filter('sf_input_object_pre', 'filter_function_name', 10, 2);

function makeDir($path)
{
     return is_dir($path) || mkdir($path);
}

function makeUrltoLink($string) {
		// The Regular Expression filter
		$reg_pattern = "/(((http|https|ftp|ftps)\:\/\/)|(www\.))[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\:[0-9]+)?(\/\S*)?/";

		// make the urls to hyperlinks
		return preg_replace($reg_pattern, '<a href="$0" target="_blank">$0</a>', $string);
}
