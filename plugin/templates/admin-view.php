<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?> 

	<div class="wrap">
		<h1 class="wp-heading-inline">Wochenübersicht: Import</h1><br>
		<hr>

<?php
if($_GET['info'] == 'failed') {

		echo '<div class="notice notice-error settings-error is-dismissible"> 
					<p><strong>Fehlgeschlagen.</strong> Bitte an den Administrator wenden.</p>
				</div>';

} elseif ($_GET['info'] == 'success') {
			echo '<div class="notice notice-success settings-error is-dismissible"> 
					<p><strong>Upload erfolgreich. Beitrag wurde erstellt.</strong></p>
				</div>';
}elseif ($_GET['info'] == 'edit') {
			echo '<div class="notice notice-success settings-error is-dismissible"> 
					<p><strong>Bearbeiten erfolgreich.</strong></p>
				</div>';
}
	?>

		<form method="POST" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" enctype="multipart/form-data">		
			<input type="hidden" name="action" value="upload_woi">

			<div id="upload-field" class="option">
				<input type="file" name="file" accept="application/JSON">
			</div>

			<div class="save-button option">
				<input type="submit" name="submit" id="submit" class="button button-primary" value="Hochladen">
			</div>
		</form>
</div>
<hr>
<style type="text/css">
	.option {
		padding: 0.5rem
	}
</style>