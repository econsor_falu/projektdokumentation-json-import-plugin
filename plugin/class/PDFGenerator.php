<?php

class PDFGenerator extends TCPDF
{
    const FONT_NAME = 'helvetica';

    protected $weekly_overview_title;
    protected $weekly_overview_year;
    protected $weekly_overview_data;

    public function __construct($title, $year, $data)
    {
        parent::__construct('L');
        date_default_timezone_set('europe/berlin');
        $this->SetMargins(13, 37, 10);
        $this->SetAutoPageBreak(true, 20);
        $this->weekly_overview_title = $title;
        $this->weekly_overview_year = $year;
        $this->weekly_overview_data = $data;
    }

    public function Header() {
        $this->Rect(12.7, 8.5, 271.5, 8, 'F', array(), array(255, 102, 0));
        $this->SetFont(self::FONT_NAME, '', 13);
        $this->SetTextColor(255, 255, 255);
        $this->SetXY(241, 10);
        $this->Write(0, '// Wochenübersicht');

        $this->SetFontSize(15);
        $this->SetTextColor(255, 102, 0);
        $this->SetXY(13.5, 20.5);
        $this->Write(0, '//');

        $this->SetTextColor(0, 102, 178);
        $this->SetXY(19, 20.5);

//        $this->Write(0, 'Wochenübersicht webBank+ ' . $this->weekly_overview_title . ' ' . $this->weekly_overview_year);
	    $this->Write(0, 'Wochenübersicht webBank+ ' . $this->weekly_overview_title);
    }

    public function Footer()
    {
        $image_file = ECONSOR_IMPORT_WEEKOVERVIEW_BASEDIR . '/assets/vr_networld_RGB_2013.jpg';
        $this->SetY(-13);
        $this->SetFont(self::FONT_NAME, '', 9);
        $this->SetTextColor(90, 90, 90);
        $this->Cell(0, 10, 'Seite ' . $this->getAliasNumPage() . ' von ' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Image($image_file, 250, 200, 36, 0, 'JPG', '', '', true);
    }

    private static function transformPfade($pfade)
    {
        return array_map(function ($pfad) {
            $prefix = '/content/vrnw/internetneu_1/website/';
            if (substr($pfad, 0, strlen($prefix)) == $prefix) {
                $pfad = substr($pfad, strlen($prefix));
            }
            return $pfad;
        }, $pfade);
    }

    private static function renderHinweis($hinweis)
    {
        if (!isset($hinweis[0])) return '';
        $image_file = '';
        switch ($hinweis[0]) {
            case 'loesch':
                $image_file = 'inhalt_wird_geloescht.png';
                break;
            case 'recht':
                $image_file = 'rechtilich_wichtiger_hinwei.png';
                break;
            case 'pruef':
                $image_file = 'inhalt_vor_veroeffentlichun.png';
                break;
            case 'individual':
                $image_file = 'inhalt_individualisieren.png';
                break;
        }
        if (empty($image_file)) return '';

        return '<img src="' . ECONSOR_IMPORT_WEEKOVERVIEW_BASEDIR . '/assets/' . $image_file . '" width="12" />';
    }

    private static function renderList($list)
    {
        $list = array_filter($list, function ($value) { return $value !== ''; } );
        if (empty($list)) {
            return '';
        }

        $html = '';
        $html .= '<ul>';
        foreach ($list as $item) {
            $html .= '<li>' . $item . '</li>';
        }
        $html .= '</ul>';
        return $html;
    }

    private static function renderInfo($entry)
    {
        $html = '';
        $html .= '<h3>' . $entry['thema'] . '</h3>';
        $html .= '<p>' . $entry['info'] . '</p>';
        return $html;
    }

    private static function renderAktion($entry, $first)
    {
        $html = '';

        if ($first) {
            $html .= '<table cellpadding="2" cellspacing="1">';
            $html .= '<tr>';
            $html .= '<th width="110">Thema</th>';
            $html .= '<th width="40">Hinweis</th>';
            $html .= '<th width="50">Termin</th>';
            $html .= '<th width="50">Aktion</th>';
            $html .= '<th width="200">Kommentar</th>';
            $html .= '<th width="200">Pfade</th>';
            $html .= '<th width="60">Zielgruppe</th>';
            $html .= '<th width="50">Tags</th>';
            $html .= '</tr>';
        }

        $html .= '<tr>';
        $html .= '<td>' . $entry['thema'] . '</td>';
        $html .= '<td>' . self::renderHinweis($entry['hinweis']) . '</td>';
        $html .= '<td>' . $entry['termin'] . '</td>';
        $html .= '<td>' . $entry['aktion'] . '</td>';
        $html .= '<td>' . $entry['kommentar'] . '</td>';
        $html .= '<td>' . self::renderList(self::transformPfade($entry['pfade'])) . '</td>';
        $html .= '<td>' . $entry['zielgruppe'] . '</td>';
        $html .= '<td>' . self::renderList($entry['tags']) . '</td>';
        $html .= '</tr>';

        return $html;
    }


    private static function renderAktion2($title, $data)
    {
        $html = '';
        $html .= '<h1>' . $title . '</h1>';
        $html .= '<table cellpadding="2" cellspacing="1">';
        $html .= '<tr>';
        $html .= '<th width="120">Thema</th>';
        $html .= '<th width="40">Hinweis</th>';
        $html .= '<th width="50">Termin</th>';
        $html .= '<th width="90">Aktion</th>';
        $html .= '<th width="200">Kommentar</th>';
        $html .= '<th width="200">Pfade</th>';
        $html .= '<th width="50">Tags</th>';
        $html .= '</tr>';
        foreach ($data as $entry) {
            $html .= '<tr>';
            $html .= '<td>' . $entry['thema'] . '</td>';
            $html .= '<td>' . self::renderHinweis($entry['hinweis']) . '</td>';
            $html .= '<td>' . $entry['termin'] . '</td>';
            $html .= '<td>' . $entry['aktion'] . '</td>';
            $html .= '<td>' . $entry['kommentar'] . '</td>';
            $html .= '<td>' . self::renderList(self::transformPfade($entry['pfade'])) . '</td>';
            $html .= '<td>' . self::renderList($entry['tags']) . '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        return $html;
    }

    private static function renderStyleTag()
    {
        $html = '';
        $html .= '<style>';
        $html .= file_get_contents(ECONSOR_IMPORT_WEEKOVERVIEW_BASEDIR . '/assets/pdf.css');
        $html .= '</style>';
        return $html;
    }

    private static function renderLegende()
    {
        $html = '';
        $html .= '<h3>Hinweis Legende</h3>';
        $html .= '<img src="' . ECONSOR_IMPORT_WEEKOVERVIEW_BASEDIR . '/assets/inhalt_individualisieren.png" width="12" />&nbsp;&nbsp;Inhalt unbedingt individualisieren <br /><br />';
        $html .= '<img src="' . ECONSOR_IMPORT_WEEKOVERVIEW_BASEDIR . '/assets/inhalt_wird_geloescht.png" width="12" />&nbsp;&nbsp;Inhalt wird gelöscht, selbst gesetzte Links und Individualisierungen prüfen <br /><br />';
        $html .= '<img src="' . ECONSOR_IMPORT_WEEKOVERVIEW_BASEDIR . '/assets/inhalt_vor_veroeffentlichun.png" width="12" />&nbsp;&nbsp;Inhalt vor Veröffentlichung unbedingt prüfen <br /><br />';
        $html .= '<img src="' . ECONSOR_IMPORT_WEEKOVERVIEW_BASEDIR . '/assets/rechtilich_wichtiger_hinwei.png" width="12" />&nbsp;&nbsp;Rechtlich wichtiger Hinweis <br /><br />';
        return $html;
    }

    public static function generatePDF1($title, $year, $data)
    {
        $pdf = new self($title, $year, $data);

        $pdf->AddPage();

        $pdf->SetFont(self::FONT_NAME, '', 9);

        $html = '';

        $openTable = false;
        foreach ($data as $entry) {

            if ($openTable && $entry['type'] != 'aktion') {
                $html .= '</table>';
                $html .= '<br pagebreak="true" />';
                $openTable = false;
            }

            switch ($entry['type']) {
                case 'info':
                    $html .= self::renderInfo($entry);
                    break;
                case 'aktion':
                    $html .= self::renderAktion($entry, !$openTable);
                    $openTable = true;
                    break;
            }
        }

        if ($openTable) {
            $html .= '</table>';
        }

        $pdf->setListIndentWidth(4);
	    $pdf->setCellHeightRatio(1.5);
        $pdf->writeHTML(self::renderStyleTag() . $html . self::renderLegende());

        return $pdf;
    }

    public static function generatePDF2($title, $year, $data)
    {
        $pdf = new self($title, $year, $data);

        $pdf->AddPage();

        $pdf->SetFont(self::FONT_NAME, '', 9);

        $html = '';
        $tableData = array();
        foreach ($data as $entry) {
            switch ($entry['type']) {
                case 'info':
                    $html .= self::renderInfo($entry);
                    break;
                case 'aktion':
                    $key = (isset($entry['zielgruppe'])) ? $entry['zielgruppe'] : '';
                    $key = strtolower(trim($key));
                    $tableData[$key][] = $entry;
                    break;
            }
        }

        $order = array('privatkunden', 'medienbibliothek', 'aktionsbereichevrnw', 'easycredit', 'firmenkunden', 'kampagnen', 'kampagnenvrnw', 'mitgliedschaft', 'uebersicht', 'wir-fuer-sie');
        $hr_names = array(
            'aktionsbereichevrnw' => 'Aktionsbereichevrnw',
            'kampagnenvrnw' => 'Kampagnen VR-NetWorld',
            'wir-fuer-sie' => 'Wir-für-Sie',
            'uebersicht' => 'Übersicht'
        );
        foreach ($order as $key) {
            if (isset($tableData[$key])){
                $title = (isset($hr_names[$key])) ? $hr_names[$key] : ucwords($key);
                $html .= '<br pagebreak="true" />';
                $html .= self::renderAktion2($title, $tableData[$key]);
            }
        }

        $rest = array_diff_key($tableData, array_flip($order));
        foreach ($rest as $key => $entry) {
            if ($key == '') continue;
            $html .= '<br pagebreak="true" />';
            $html .= self::renderAktion2(ucwords($key), $entry);
        }

        if (isset($tableData[''])) {
            $html .= '<br pagebreak="true" />';
            $html .= self::renderAktion2('Ohne Zielgruppe', $tableData['']);
        }

        $pdf->setListIndentWidth(4);
	    $pdf->setCellHeightRatio(1.5);
        $pdf->writeHTML(self::renderStyleTag() . $html . self::renderLegende());

        return $pdf;
    }
}