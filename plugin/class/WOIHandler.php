<?php

class WOIHandler {

  	/**
     * Surround given content with avia shortcode (one third template)
     * @var string
     */
	function layoutOneThird($content) {
		$ret = " ";

		$ret .= "[av_one_third first  min_height='' vertical_alignment='av-align-top' space='' row_boxshadow='' row_boxshadow_color='' row_boxshadow_width='10' custom_margin='' margin='0px' mobile_breaking='' border='' border_color='' radius='0px' padding='0px' column_boxshadow='' column_boxshadow_color='' column_boxshadow_width='10' background='bg_color' background_color='' background_gradient_color1='' background_gradient_color2='' background_gradient_direction='vertical' src='' background_position='top left' background_repeat='no-repeat' highlight='' highlight_size='' animation='' link='' linktarget='' link_hover='' title_attr='' alt_attr='' mobile_display='' id='' custom_class='' template_class='' aria_label='' element_template='' one_element_template='' show_locked_options_fakeArg='' av_uid='' sc_version='1.0']

			[av_textblock size='' av-medium-font-size='' av-small-font-size='' av-mini-font-size='' font_color='' color='' id='' custom_class='' template_class='' element_template='' one_element_template='' av_uid='av-knhfemoi' sc_version='1.0' admin_preview_bg='']
			";

		$ret .= $content;

		$ret .= "[/av_textblock][/av_one_third]";
		return $ret;
	}

  	/**
     * Surround given content with avia shortcode (two third template)
     * @var string
     */
	function layoutTwoThird($content) {
		$ret = " ";

		$ret .= "[av_two_third  min_height='' vertical_alignment='av-align-top' space='' row_boxshadow='' row_boxshadow_color='' row_boxshadow_width='10' custom_margin='' margin='0px' mobile_breaking='' border='' border_color='' radius='0px' padding='0px' column_boxshadow='' column_boxshadow_color='' column_boxshadow_width='10' background='bg_color' background_color='' background_gradient_color1='' background_gradient_color2='' background_gradient_direction='vertical' src='' background_position='top left' background_repeat='no-repeat' highlight='' highlight_size='' animation='' link='' linktarget='' link_hover='' title_attr='' alt_attr='' mobile_display='' id='' custom_class='' template_class='' aria_label='' element_template='' one_element_template='' show_locked_options_fakeArg='' av_uid='' sc_version='1.0']";

		$ret .= $content;

		$ret .= "[/av_two_third]";
		return $ret;
	}


	 /**
     * create tab for toggle container
     * @var string,string
     */
	function createToggleTab($content,$title,$id) {
		$ret = " ";
		$ret .= "[av_toggle title='{$title}' tags=''av_uid='' custom_id='{$id}']";
		$ret .= $content;
		$ret .= "[/av_toggle]";

		//var_dump($ret);
		//die();

		return $ret;
		
	}

	 /**
     * Surround given tabs as string with avia shortcode (akkordeon)
     * @var string
     */
	function createToggleContainer($content) {
		$ret = " ";

		$ret .= "[av_toggle_container faq_markup='' initial='1' mode='accordion' sort='' styling='av-elegant-toggle' colors='custom' font_color='#333333' background_color='#f2f2f2' border_color='#333333' colors_current='custom' font_color_current='#ff6600' background_current='bg_color' background_color_current='#f2f2f2' background_gradient_current_color1='' background_gradient_current_color2='' background_gradient_current_direction='vertical' hover_colors='custom' hover_background_color='#f2f2f2' hover_font_color='#ff6600' alb_description='' id='wo-accordeon' custom_class='wo-accordeon' template_class='' one_element_template='' av_uid='' sc_version='1.0']";

		$ret .= $content;

		$ret .= "[/av_toggle_container]";

		return $ret;
	} 


	/**
     * Surround with intro section
     * @var string
     */
	function introsection($content) {
		$ret = " ";

		$ret .= "[av_section min_height='' min_height_pc='25' min_height_px='500px' padding='default' custom_margin='0px' custom_margin_sync='true' color='main_color' background='bg_color' custom_bg='#f2f2f2' background_gradient_color1='' background_gradient_color2='' background_gradient_direction='vertical' src='/wp-content/themes/vr-networld/img/files-weekly-overview-big.svg' attachment='' attachment_size='' attach='scroll' position='top left' repeat='no-repeat' video='' video_ratio='16:9' overlay_opacity='0.5' overlay_color='' overlay_pattern='' overlay_custom_pattern='' shadow='no-border-styling' bottom_border='no-border-styling' bottom_border_diagonal_color='#333333' bottom_border_diagonal_direction='' bottom_border_style='' custom_arrow_bg='' id='' custom_class='intro' aria_label='' av_element_hidden_in_editor='0' av_uid='']";

		$ret .= $content;

		$ret .= "[/av_section]";

		return $ret;
	}

	/**
     * Surround with section
     * @var string
     */
	function section($content) {
		$ret = " ";

		$ret .= "[av_section min_height='' min_height_pc='25' min_height_px='500px' padding='default' margin='' custom_margin='0px' color='main_color' background='bg_color' custom_bg='' background_gradient_color1='' background_gradient_color2='' background_gradient_direction='vertical' src='' attach='scroll' position='top left' repeat='no-repeat' video='' video_ratio='16:9' video_mobile_disabled='' overlay_enable='' overlay_opacity='0.5' overlay_color='' overlay_pattern='' overlay_custom_pattern='' shadow='no-border-styling' bottom_border='no-border-styling' bottom_border_diagonal_color='#333333' bottom_border_diagonal_direction='' bottom_border_style='' scroll_down='' custom_arrow_bg='' av-desktop-hide='' av-medium-hide='' av-small-hide='' av-mini-hide='' id='' custom_class='' template_class='' aria_label='' element_template='' one_element_template='' show_locked_options_fakeArg='' av_element_hidden_in_editor='0' av_uid='' sc_version='1.0']";

		$ret .= $content;

		$ret .= "[/av_section]";

		return $ret;
	}


 	/**
     * Surround given content with infocard-div
     * @var string
     */
	function createInfoCard($heading,$date,$note,$tags,$action,$path,$target,$comment,$count) {
		$ret = " ";
		$extraclass = ($count % 2 ? 'odd': 'even');
		$ret .= "<div class='infocard {$extraclass} '>";

		$ret .= "		<div class='infocard-header'>";
		$ret .= "			<div class='infocard-heading'>";
		$ret .= 				$heading;
		$ret .= "			</div>";
		$ret .= "			<div class='infocard-date'>";
		$ret .= "				<small>Termin: ".$date."</small>";
		$ret .= "			</div>";
		$ret .= "		</div>";


		$ret .= "		<div class='infocard-body'>";
		$ret .= "			<div class='infocard-note'>";
		$ret .= "				<p><strong>Hinweis:</strong> ".$note."</p>";
		$ret .= "			</div>";
		$ret .= "			<div class='infocard-tags'>";
		$ret .= "				<p><strong>Tags:</strong> ".$tags."</p>";
		$ret .= "			</div>";

		$ret .= "			<div class='infocard-action'>";
		$ret .= "				<p><strong>Aktion:</strong> ".$action."</p>";
		$ret .= "			</div>";
		$ret .= "			<div class='infocard-path'>";
		$ret .= "				<p><strong>Pfade:</strong> ".$path."</p>";
		$ret .= "			</div>";

		$ret .= "			<div class='infocard-target'>";
		$ret .= "				<p><strong>Zielgruppe:</strong> ".$target."</p>";
		$ret .= "			</div>";
		$ret .= "		</div>";


		$ret .= "		<div class='infocard-footer'>";
		$ret .= "			<p class='infocard-comment'>";
		$ret .= "				<strong>Kommentar</strong>".$comment."";
		$ret .= "			</p>";
		$ret .= "		</div>";

		$ret .= "</div>";

		return $ret;
	}


 	/**
     * Surround given tabs as string with avia shortcode (akkordeon)
     * @var string
     */
	function createHeading($content) {
		$ret = " ";

		$ret .= "[av_one_full first  min_height='' vertical_alignment='av-align-top' space='' row_boxshadow='' row_boxshadow_color='' row_boxshadow_width='10' custom_margin='' margin='0px' mobile_breaking='' border='' border_color='' radius='0px' padding='0px' column_boxshadow='' column_boxshadow_color='' column_boxshadow_width='10' background='bg_color' background_color='' background_gradient_color1='' background_gradient_color2='' background_gradient_direction='vertical' src='' background_position='top left' background_repeat='no-repeat' highlight='' highlight_size='' animation='' link='' linktarget='' link_hover='' title_attr='' alt_attr='' mobile_display='' id='' custom_class='' template_class='' aria_label='' element_template='' one_element_template='' show_locked_options_fakeArg='' av_uid='' sc_version='1.0']

			[av_textblock size='' av-medium-font-size='' av-small-font-size='' av-mini-font-size='' font_color='' color='' id='' custom_class='' template_class='' element_template='' one_element_template='' av_uid='av-knhfemoi' sc_version='1.0' admin_preview_bg='']";

		$ret .= $content;

		$ret .= "[/av_textblock]
					[/av_one_full]";

		return $ret;
	}


	/**
	* convert calendar week to range
	* @var string
	*/
	function convertCWeek($week, $year) {
		$dto = new DateTime();
		$dto->setISODate($year, $week);
		$ret['week_start'] = $dto->format('d-m-Y');
		$dto->modify('+6 days');
		$ret['week_end'] = $dto->format('d-m-Y');
		$ret['month'] = $dto->format('m');
		return $ret;
	}

	/**
	* convert calendar week to range
	* @var string
	*/
	function getMonth($m) {
		$d = array(
					1 => 'Januar',
					2 => 'Februar',
					3 => 'März',
					4 => 'April',
					5 => 'Mai',
					6 => 'Juni',
					7 => 'Juli',
					8 => 'August',
					9 => 'September',
					10 => 'Oktober',
					11 => 'November',
					12 => 'Dezember',
			);

		return $d[(int) $m];
	}




}