$.fn.isOnScreen = function(){

    var win = $(window);

    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

};




jQuery(document).ready(function() {

	var current = jQuery('.activeTitle').attr('aria-controls');
	jQuery('span[data-target="#'+current+'"]').toggleClass('active');


	jQuery('.wo-accordeon > section').on("click", function() {
		jQuery('span[data-target="#'+current+'"]').toggleClass('active');
		current = jQuery('.activeTitle').attr('aria-controls');
		jQuery('span[data-target="#'+current+'"]').toggleClass('active');
	})



	if(jQuery(window).width() <= 1024) {
		jQuery('.content-table').hide();
		jQuery('.content-table').prev().andSelf().wrapAll('<div class="mobile-contentable-wrapper">')

		jQuery('.heading-contenttable').addClass('mobile-contenttable').on('click', function() {
			if(jQuery('.content-table').is(':visible')) {
				jQuery('.content-table').slideUp(1000,'swing');
				jQuery('.heading-contenttable').removeClass('active');
			} else {
				jQuery('.content-table').slideDown(1000,'swing');
				jQuery('.heading-contenttable').addClass('active');
			}
		})
	} else {

		jQuery('.heading-contenttable').insertBefore(jQuery('.content-entry')[0]);

		jQuery(window).on('scroll', function() {
			if(jQuery('.sticky-trigger').isOnScreen()) {
				jQuery('.content-table').removeClass('sticky');
				//jQuery('.heading-contenttable').insertBefore(jQuery('.content-table')[0]);
			} else {
				jQuery('.content-table').addClass('sticky');
				//jQuery('.heading-contenttable').insertBefore(jQuery('.content-entry')[0]);
			}
		});

	}

	jQuery('.content-table').css('width',jQuery('.content-table').closest('section').width());


	if(window.location.hash) {
	 	var target = window.location.hash;

		if(jQuery(window).width() <= 1024) {
			var offsetValue = 100;
		} else {
			var offsetValue = 178;
		}

		setTimeout(function() {
			jQuery('html:not(:animated),body:not(:animated)').animate({ scrollTop: jQuery(target).parent().parent().offset().top - offsetValue }, 1200, 'easeInOutQuint');
		}, 350);
	} 



	jQuery('.content-nav').on('click', function() {
		var target = jQuery(this).data('target');
		jQuery(target).parent().find('p').click();

		if(jQuery(window).width() <= 1024) {
			var offsetValue = 100;
		} else {
			var offsetValue = 178;
		}
		setTimeout(function() {
			jQuery('html:not(:animated),body:not(:animated)').animate({ scrollTop: jQuery(target).parent().parent().offset().top - offsetValue }, 1200, 'easeInOutQuint');
		}, 350);

	});

});




