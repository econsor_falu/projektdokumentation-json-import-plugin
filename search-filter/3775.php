<?php
/**
 * Search & Filter Pro 
 *
 * Resultpage for detailed week overview search
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}



require_once(get_stylesheet_directory().'/Econsor-searcher.php');

if ( $query->have_posts() )
{


	
	?>
	<?php 
		/*
		<div class="pagination">
			
			<div class="nav-previous"><?php next_posts_link( 'Older posts', $query->max_num_pages ); ?></div>
			<div class="nav-next"><?php previous_posts_link( 'Newer posts' ); ?></div>
			<?php
				/// example code for using the wp_pagenavi plugin 
				if (function_exists('wp_pagenavi'))
				{
					echo "<br />";
					wp_pagenavi( array( 'query' => $query ) );
				}
			?>
		</div>
		*/ 
	?>

	<div class="card-wrapper">	
	<?php

	$results = array();
	while ($query->have_posts())
	{
		$query->the_post();
		$search = $query->query['s'];
		

		$currentPost = $query->posts[$query->current_post];
		$title 	= $currentPost->post_title; 
		$url 	= $currentPost->guid;

        $searcher = new ECSearcher();
        $res = $searcher->getDetailedWeekoverview($currentPost->ID,$search);
        foreach($res as $r) {
        	array_push($results, $r);    	
        }

	}

    if(!empty($results)) {
		foreach ($results as $result) {
			echo $result ;			
		}
	} else {
		echo 'Keine Einträge für den Suchbegriff "'.$search.'" gefunden.';
	}

	

	?>
	</div>

	<div class="pagination">
		
		<div class="nav-previous"><?php next_posts_link( '<', $query->max_num_pages ); ?></div>
		<div class="nav-next"><?php previous_posts_link( '>' ); ?></div>
		<?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
	</div>
	<?php
}
else
{
	$search = $query->query['s'];
	echo '<div class="card-wrapper">Keine Einträge für den Suchbegriff "'.$search.'" gefunden.</div>';
}
?>